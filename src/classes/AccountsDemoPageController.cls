public with sharing class AccountsDemoPageController {
	public list<account> accounts{get;set;}

	public AccountsDemoPageController() {

		accounts = new List<Account>();
		accounts = AccountService.getAccounts();
		
	}
}